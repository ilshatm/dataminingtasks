﻿using System;
using System.Collections.Generic;

namespace Task1
{
    class Group : IComparable<Group>
    {
        public string Name { get; set; }
        public string Theme { get; set; }


        public int CompareTo(Group obj)
        {
            return Theme.CompareTo(obj.Theme);
        }
    }
    class User
    {
        public string Name { get; set; }
        public List<Group> Groups { get; set; }
        public List<User> Friends { get; set; }
        
        
    }

    
    class Program
    {
        static bool CheckForInterests(User user1, User user2)
        {
            var b = false;
            foreach (var i in user1.Groups)
            {
                foreach(var j in user2.Groups)
                {
                    if (j.Theme == i.Theme)
                    {
                        b = true;
                    }
                }
            }
            return b;
        }
        static void Main(string[] args)
        {
            User user1 = new User()
            {
                Name = "Ivan",
                Groups = new List<Group>()
                {
                    new Group(){Name="SuperCars", Theme = "Cars"},
                    new Group(){Name="SuperSport", Theme = "Sport"},
                }


            };
            User user2 = new User()
            {
                Name = "Nikolai",
                Groups = new List<Group>()
                {
                    new Group(){Name="4Game", Theme = "Games"},
                    new Group(){Name="dev", Theme = "IT"},
                }


            };
            User user3 = new User()
            {
                Name = "Vadim",
                Groups = new List<Group>()
                {
                    new Group(){Name="4Sport", Theme = "Sport"},
                    new Group(){Name="dev", Theme = "IT"},
                }
            };
            User ME = new User()
            {
                Name = "Ilshat",
                Groups = new List<Group>()
                {
                    new Group(){Name="4Game", Theme = "Games"},
                    new Group(){Name="AnimeNaruto", Theme = "Anime"},
                    new Group(){Name="dev", Theme = "IT"}
                }


            };
            ME.Friends = new List<User>() { user3, user1 };
            foreach(var i in ME.Friends)
            {
                if (CheckForInterests(i, ME))
                {
                    Console.WriteLine(i.Name);
                }
            }
        }
    }
}
