﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Task3
{
    class Program
    {
        static void FillMaxRight(List<int> ls, int pos)
        {
            if (pos < ls.Count - 1)
            {
                var max = 0;
                var position = 0;
                for (int i = pos + 1; i < ls.Count; i++)
                {
                    if (ls[i] > max)
                    {
                        max = ls[i];
                        position = i;
                    }
                }
                for (int i = position - 1; i > pos; i--)
                {
                    ls[i] += max - ls[i];
                }
                FillMaxRight(ls, position);
            }
        }
        static void FillMaxLeft(List<int> ls, int pos)
        {
            if (pos > 0)
            {
                var max = 0;
                var position = 0;
                for (int i = pos -1; i >= 0; i--)
                {
                    if (ls[i] > max)
                    {
                        max = ls[i];
                        position = i;
                    }
                }
                for (int i = position + 1; i < pos; i++)
                {
                    ls[i] += max - ls[i];
                }
                FillMaxLeft(ls, position);
            }
        }
        static void Main(string[] args)
        {
            Console.SetWindowSize(Console.LargestWindowWidth, Console.LargestWindowHeight);
            int baskets=0;
            StreamReader sr = new StreamReader("transactions.csv");
            Dictionary<string, int> prod = new Dictionary<string, int>();
            string s;
            sr.ReadLine();
            string past = " ";
            while ((s = sr.ReadLine()) != null)
            {
                
                string[] str = s.Split(';');
                if (past != str[1])
                {
                    baskets++;
                }
                try
                {
                    prod[str[0]]++;
                }
                catch
                {
                    prod.Add(str[0], 1);
                    
                }
                past = str[1];
            }
            sr.Close();
            List<int> ls = new List<int>();
            foreach(var i in prod)
            {
                ls.Add(i.Value);
            }
            var max = 0;
            int position=0;
            for(int i = 0; i < ls.Count; i++)
            {
                if (ls[i] > max)
                {
                    max = ls[i];
                    position = i;
                }
            }
            FillMaxRight(ls, position);
            FillMaxLeft(ls, position);
            var j = 0;
            Dictionary<string, int> result = new Dictionary<string, int>();
            foreach (var i in prod)
            {
                result.Add(i.Key, ls[j]);
                j++;
            }
            foreach(var i in result)
            {
                Console.Write(i.Key + "("+i.Value+"):");
                Console.ForegroundColor = ConsoleColor.Green;
                for (int k = 0; k < i.Value; k+=5)
                {
                    Console.Write("|");
                }
                Console.ResetColor();
                Console.WriteLine();
            }
            
            


            
        }
    }
}
