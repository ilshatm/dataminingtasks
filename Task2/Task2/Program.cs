﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Drawing.Drawing2D;

namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.SetWindowSize(Console.LargestWindowWidth, Console.LargestWindowHeight);
            StreamReader sr = new StreamReader("transactions.csv");
            Dictionary<string, int> prod = new Dictionary<string, int>();
            string s;
            sr.ReadLine();
            while((s = sr.ReadLine()) != null)
            {
                string[] str = s.Split(';');
                try
                {
                    prod[str[0]]++;
                }
                catch
                {
                    prod.Add(str[0], 1);
                }
            }
            sr.Close();
            foreach (var i in prod)
            {
                Console.Write(i.Key + "("+i.Value+"):");
                Console.ForegroundColor = ConsoleColor.Green;
                for(int j = 0; j < i.Value; j+=3)
                {
                    
                    Console.Write("|");

                }
                Console.ResetColor();
                Console.WriteLine();
            }
            
            
        }
    }
}
